import { NetworkType } from 'tsjs-xpx-chain-sdk';
import * as config from '../../config.json';

export class SiriusConstants {
  public static generationHash: string = config.nodeInfo.generationHash;
  public static nodeUrl: string = config.nodeInfo.nodeUrl;
  public static wsNodeUrl: string = config.nodeInfo.wsNodeUrl;
	public static networkType: NetworkType = NetworkType[config.nodeInfo.networkType];
	public static mosaicHexId = config.nodeInfo.mosaicHexId;

	public static checkTransactionTypes: string[] = config.nodeInfo.checkTransactionTypes;
	public static blockedTransactionTypes: string[] = config.nodeInfo.blockedTransactionTypes;
}
