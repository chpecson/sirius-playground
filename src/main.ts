import * as config from '../config.json';
import { MultisigConversion } from './transaction/multisig-conversion';
import { MosaicCreation } from "./transaction/mosaic-creation";
import { SimpleTransfer } from './transaction/simple-transfer';
import { GetGenesisBlock } from "./transaction/get-genesis-block";
import { MultisigAggregateComplete } from "./transaction/multisig-aggregate-complete";
import { AccountBalanceCheck } from './transaction/account-balance-check';
import { ChainConfigUpdate } from './transaction/chainConfigUpdate.js';

if(config.testFeatures.multisigConversion) {
	const multisigConversion = new MultisigConversion();
	multisigConversion.initConversion();
}

if(config.testFeatures.mosaicCreation.enabled) {
	const mosaicCreation = new MosaicCreation();
	mosaicCreation.init();
}

if(config.testFeatures.simpleTransfer) {
	const simpleTransfer = new SimpleTransfer();
	simpleTransfer.init();
}

if(config.testFeatures.getGenesisBlock) {
	new GetGenesisBlock().init();
}

if(config.testFeatures.multisigAggregateComplete) {
	const multisigAggregateComplete = new MultisigAggregateComplete();
	multisigAggregateComplete.initWithSerialize();
}

if(config.testFeatures.accountBalanceCheck) {
	const accountBalanceCheck = new AccountBalanceCheck;
	accountBalanceCheck.init();
}

if(config.testFeatures.chainConfigUpdate.enabled) {
	const chainConfigUpdate = new ChainConfigUpdate;
	chainConfigUpdate.init();
}
