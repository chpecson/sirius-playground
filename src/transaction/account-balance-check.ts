import { AccountHttp, Address, Account } from "tsjs-xpx-chain-sdk";
import { SiriusConstants } from "../@constants/sirius.constant";

export class AccountBalanceCheck {
	public init() {
		const accountHttp = new AccountHttp(SiriusConstants.nodeUrl);
    const account = Account.createFromPrivateKey('89E6052886B5822223238F04FDA1246D61930FE69A90A1DDAE6131F948558024', SiriusConstants.networkType);
		const address = account.address;
		accountHttp.getAccountInfo(address).subscribe(accountInfo => {

			console.log('TCL: AccountBalanceCheck -> init -> address', address.pretty());
			accountInfo.mosaics.forEach(mosaic => {
      	console.log('TCL: AccountBalanceCheck -> init -> mosaic', mosaic.id.toHex(), '===', mosaic.id, '\n');
			});

		});
	}
}
