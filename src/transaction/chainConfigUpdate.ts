import {
	Account,
	BlockHttp,
	BlockInfo,
	Deadline,
	MosaicDefinitionTransaction,
	QueryParams,
	RegisterNamespaceTransaction,
	SignedTransaction,
	TransactionHttp,
	TransactionInfo,
	TransactionType,
	TransferTransaction,
	UInt64,
	ChainHttp
} from 'tsjs-xpx-chain-sdk';
import { ChainConfigTransaction } from 'tsjs-xpx-chain-sdk/dist/src/model/transaction/ChainConfigTransaction';

import { SiriusConstants } from '../@constants/sirius.constant';
import * as config from '../../config.json';

class NemesisBlockInfo {
	private static instance: BlockInfo;
	private constructor() {}
	static async getInstance(): Promise<BlockInfo> {
		if (!NemesisBlockInfo.instance) {
			const blockHttp = new BlockHttp(SiriusConstants.nodeUrl);
			NemesisBlockInfo.instance = await blockHttp
				.getBlockByHeight(1)
				.toPromise();
		}
		return NemesisBlockInfo.instance;
	}
}

const GetNemesisBlockDataPromise = () => {
	const blockHttp = new BlockHttp(SiriusConstants.nodeUrl);
	return NemesisBlockInfo.getInstance().then(nemesisBlockInfo => {
		return blockHttp
			.getBlockTransactions(1, new QueryParams(100))
			.toPromise()
			.then(txs => {
				const regNamespaceTxs = txs.filter(
					tx => tx.type === TransactionType.REGISTER_NAMESPACE
				) as RegisterNamespaceTransaction[];
				const currencyNamespace = regNamespaceTxs.find(
					tx => tx.namespaceName === 'xpx'
				);
				const testNamespace = currencyNamespace
					? currencyNamespace
					: regNamespaceTxs[0];
				const regMosaicTx = txs.find(
					tx => tx.type === TransactionType.MOSAIC_DEFINITION
				) as MosaicDefinitionTransaction;
				const transferTx = txs.find(
					tx => tx.type === TransactionType.TRANSFER
				) as TransferTransaction;
				const config = txs.find(
					tx => tx.type === TransactionType.CHAIN_CONFIGURE
				) as ChainConfigTransaction;

				return {
					nemesisBlockInfo,
					testNamespace: {
						Id: testNamespace.namespaceId,
						Name: testNamespace.namespaceName
					},
					otherTestNamespace: {
						Id: regNamespaceTxs[0].namespaceId,
						Name: regNamespaceTxs[0].namespaceName
					},
					testTxHash: (transferTx.transactionInfo as TransactionInfo)
						.hash as string,
					testTxId: (transferTx.transactionInfo as TransactionInfo).id,
					config
				};
			});
	});
};

export class ChainConfigUpdate {
	async init() {
		const account = Account.createFromPrivateKey(config.testFeatures.chainConfigUpdate.nemesisPrivateKey, SiriusConstants.networkType);
		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		GetNemesisBlockDataPromise().then(async nemesisBlockInfo => {
			let blockChainConfig = nemesisBlockInfo.config.blockChainConfig;

			blockChainConfig = blockChainConfig.replace(/maxFields\s\=\s10/gi, 'maxFields = 20');
			blockChainConfig = blockChainConfig.replace(/maxFieldValueSize\s\=\s1024/gi, 'maxFieldValueSize = 10240');
			blockChainConfig = blockChainConfig.replace(/maxMultisigDepth\s\=\s0/gi, 'maxMultisigDepth = 15');
			blockChainConfig = blockChainConfig.replace(/maxCosignersPerAccount\s\=\s10/gi, 'maxCosignersPerAccount = 15');
			blockChainConfig = blockChainConfig.replace(/mosaicRentalFee\s\=\s500\'000\'000/gi, 'mosaicRentalFee = 0');
			blockChainConfig = blockChainConfig.replace(/rootNamespaceRentalFeePerBlock\s\=\s100\'000/gi, 'rootNamespaceRentalFeePerBlock = 0');
			blockChainConfig = blockChainConfig.replace(/lockedFundsPerAggregate\s=\s10\'000\'000/gi, 'lockedFundsPerAggregate = 0');

			const currentChainHeightUint64 = await this.getChainHeight();
			const currentChainHeight = currentChainHeightUint64.compact();
			const applyOnChainHeight = currentChainHeight + 20;
			const applyOnChainHeightUint64 = UInt64.fromUint(applyOnChainHeight);
			const chainConfigTransaction = ChainConfigTransaction.create(
				Deadline.create(),
				applyOnChainHeightUint64,
				blockChainConfig,
				nemesisBlockInfo.config.supportedEntityVersions,
				SiriusConstants.networkType
			);

			const signedTransaction = chainConfigTransaction.signWith(account, SiriusConstants.generationHash);

			console.log('TCL: ChainConfigUpdate -> init -> chainConfigTransaction', chainConfigTransaction);
			console.log('TCL: ChainConfigUpdate -> init -> signedTransaction', signedTransaction);

			this.checkTxnStatus(signedTransaction);
			transactionHttp.announce(signedTransaction).subscribe(response => {
				console.log('TCL: ChainConfigUpdate -> init -> response', response);
			});
		});
	}

	checkTxnStatus(txn: SignedTransaction) {
		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		const interval = setTimeout(() => {
			transactionHttp.getTransactionStatus(txn.hash).subscribe(
				status => {
					console.log('TCL: ChainConfigUpdate -> init -> status ', status);
					if (status.group === 'confirmed') {
						clearTimeout(interval);
					} else {
						this.checkTxnStatus(txn);
					}
				},
				err => {
					console.error('TCL: ChainConfigUpdate -> init -> err', err);
				}
			);
		}, 5000);
	}

	async getChainHeight() {
		const chainHttp = new ChainHttp(SiriusConstants.nodeUrl);
		return await chainHttp.getBlockchainHeight().toPromise();
	}
}
