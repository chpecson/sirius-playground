import { filter, mergeMap, toArray } from 'rxjs/operators';
import {
  AggregateTransaction,
  BlockHttp,
  TransactionType,
  TransferTransaction
} from 'tsjs-xpx-chain-sdk';

import { SiriusConstants } from '../@constants/sirius.constant';

export class GetGenesisBlock {
  init() {
    const blockHttp = new BlockHttp(SiriusConstants.nodeUrl, null);
    blockHttp.getBlockTransactions(1).pipe(
        mergeMap(txn => txn),
        filter(txn => txn.type === TransactionType.TRANSFER),
        toArray()
      ).subscribe((txns: AggregateTransaction[]) => {
        txns.forEach(txn => {
					console.log('TCL: GetGenesisBlock -> init -> txn', txn);

          if (txn.type === TransactionType.TRANSFER) {
            const transferTxn = <TransferTransaction>(<any>txn);
            transferTxn.mosaics.forEach(mosaic => {
              console.log('TCL: SimpleTransfer -> init -> mosaic', mosaic.id.toHex());
              console.log('TCL: SimpleTransfer -> init -> mosaic', mosaic);
            });
          }
        });
      });
  }
}
