import { ChronoUnit } from 'js-joda';
import {
  Account,
  AggregateTransaction,
  AliasActionType,
  AliasTransaction,
  Deadline,
  MosaicId,
  MosaicNonce,
  MosaicProperties,
  MosaicSupplyChangeTransaction,
  MosaicSupplyType,
  NamespaceId,
  RegisterNamespaceTransaction,
  SignedTransaction,
  TransactionHttp,
  UInt64,
} from 'tsjs-xpx-chain-sdk';
import { MosaicDefinitionTransaction } from 'tsjs-xpx-chain-sdk/dist/src/model/transaction/MosaicDefinitionTransaction';

import { SiriusConstants } from '../@constants/sirius.constant';
import * as config from '../../config.json';

export class MosaicCreation {
	public init() {
		config.testFeatures.mosaicCreation.mosaics.forEach(mosaic => {
			this.createMosaic(mosaic.name, mosaic.properties.supplyMutable, mosaic.properties.transferable, mosaic.properties.divisibility, mosaic.supply);
		});
	}

	initWithAlias() {
		 /* start block 01 */
		 const namespaceName = "amc";
		 const subnamespaceName = "amc";

		 const registerNamespaceTransaction = RegisterNamespaceTransaction.createRootNamespace(
			Deadline.create(15, ChronoUnit.MINUTES),
			 namespaceName,
			 undefined,
			 SiriusConstants.networkType
		 );
		 const subNamespaceTransaction = RegisterNamespaceTransaction.createSubNamespace(
			Deadline.create(15, ChronoUnit.MINUTES),
			namespaceName,
			subnamespaceName,
			SiriusConstants.networkType
		);

		const aliasForMosaicTxn = AliasTransaction.createForMosaic(
			Deadline.create(15, ChronoUnit.MINUTES),
			AliasActionType.Link,
			new NamespaceId('amc.amc'),
			new MosaicId('4a582a4d82139bee'),
			SiriusConstants.networkType
		);

		const msu = Account.createFromPrivateKey('F9B57521DCE628F3619AD47D6310862A213A879CDED3CC885300E5EEEC596A63', SiriusConstants.networkType);
		const userAccount = Account.createFromPrivateKey('A9C30405D864E99608CE82997B851441FECFCB52D0ACCD29DD7995B5EF6DB613', SiriusConstants.networkType);
		const aggregateComplete: AggregateTransaction = AggregateTransaction.createComplete(
			Deadline.create(15, ChronoUnit.MINUTES),
			[
				registerNamespaceTransaction.toAggregate(msu.publicAccount),
				subNamespaceTransaction.toAggregate(msu.publicAccount),
				aliasForMosaicTxn.toAggregate(msu.publicAccount)
			],
			SiriusConstants.networkType,
			[]
		);
		const signedAggregateComplete = aggregateComplete.signTransactionWithCosignatories(userAccount, [], SiriusConstants.generationHash);
		console.log('TCL: MosaicCreation -> initWithAlias -> signedAggregateComplete', signedAggregateComplete);

		this.checkTxnStatus(signedAggregateComplete);
		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		transactionHttp.announce(signedAggregateComplete).subscribe(x => console.log(x), err => console.error(err));
	}

	initMosaicDefinition() {
		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
    const account = Account.createFromPrivateKey('89E6052886B5822223238F04FDA1246D61930FE69A90A1DDAE6131F948558024', SiriusConstants.networkType);
    console.log('TCL: MosaicCreation -> init -> account.address', account.address.plain());
    console.log('TCL: MosaicCreation -> init -> account.publicKey', account.publicKey);
    console.log('TCL: MosaicCreation -> init -> account.privateKey', account.privateKey);

    const nonce = MosaicNonce.createRandom();
    const mosaicDefinitionTransaction = MosaicDefinitionTransaction.create(
      Deadline.create(),
      nonce,
      MosaicId.createFromNonce(nonce, account.publicAccount),
      MosaicProperties.create({ supplyMutable: true, transferable: true, divisibility: 6, duration: undefined }),
			SiriusConstants.networkType,
			UInt64.fromUint(0)
		);
		const signedTxn = account.sign(mosaicDefinitionTransaction, SiriusConstants.generationHash);
		console.log('TCL: MosaicCreation -> initMosaicDefinition -> signedTxn', signedTxn);
		this.checkTxnStatus(signedTxn);

		transactionHttp.announce(signedTxn).subscribe((res) => {
    	console.log('TCL: MosaicCreation -> initMosaicDefinition -> res', res);
		});
	}

	checkTxnStatus(txn: SignedTransaction) {
		 const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		 const interval = setInterval(() => {
			transactionHttp.getTransactionStatus(txn.hash).subscribe(status => {
				console.log('TCL: MosaicCreation -> init -> status ', status);
				if(status.group === 'confirmed') {
					clearInterval(interval);
				}
			}, err => {
      	console.error('TCL: MosaicCreation -> init -> err', err);
			});
		}, 5000);
	}

	createMosaic(name: string, supplyMutable: boolean, transferable: boolean, divisibility: number, supply: number) {
		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
    const account = Account.createFromPrivateKey('89E6052886B5822223238F04FDA1246D61930FE69A90A1DDAE6131F948558024', SiriusConstants.networkType);

		const nonce = MosaicNonce.createRandom();
		const mosaicId = MosaicId.createFromNonce(nonce, account.publicAccount);
    console.log('TCL: MosaicCreation -> createMosaic -> name', name, ' -> mosaicId -> ', mosaicId);
    const mosaicDefinitionTransaction = MosaicDefinitionTransaction.create(
      Deadline.create(),
      nonce,
      mosaicId,
      MosaicProperties.create({ supplyMutable, transferable, divisibility, duration: undefined }),
			SiriusConstants.networkType,
			new UInt64([0, 0])
		);
    /* end block 01 */

    /* start block 02 */
    const mosaicAmount = supply * Math.pow(10, divisibility);
    const mosaicSupplyChangeTransaction = MosaicSupplyChangeTransaction.create(
      Deadline.create(),
      mosaicDefinitionTransaction.mosaicId,
      MosaicSupplyType.Increase,
      UInt64.fromUint(mosaicAmount),
			SiriusConstants.networkType,
			new UInt64([0, 0])
    );
    /* end block 02 */

    /* start block 03 */
    const aggregateTransaction = AggregateTransaction.createComplete(
      Deadline.create(),
      [
        mosaicDefinitionTransaction.toAggregate(account.publicAccount),
        mosaicSupplyChangeTransaction.toAggregate(account.publicAccount)
      ],
      SiriusConstants.networkType,
			[]
		);

    const signedTransaction = account.sign(aggregateTransaction, SiriusConstants.generationHash);
    console.log('signedTransaction', signedTransaction);

		this.checkTxnStatus(signedTransaction);

    transactionHttp.announce(signedTransaction).subscribe(x => console.log(x), err => console.error(err));
	}
}
