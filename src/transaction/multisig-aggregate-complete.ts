import { ChronoUnit } from 'js-joda';
import {
  Account,
  Address,
  AggregateTransaction,
  CosignatureSignedTransaction,
  CosignatureTransaction,
  Deadline,
  EmptyMessage,
  InnerTransaction,
  Mosaic,
  MosaicId,
  PublicAccount,
  TransactionHttp,
  TransactionMapping,
  TransferTransaction,
  UInt64,
  SignedTransaction,
} from 'tsjs-xpx-chain-sdk';

import { SiriusConstants } from '../@constants/sirius.constant';

export class MultisigAggregateComplete {
	init() {
		const msxAccount = Account.createFromPrivateKey('89E6052886B5822223238F04FDA1246D61930FE69A90A1DDAE6131F948558024', SiriusConstants.networkType);
		const userAccount = Account.createFromPrivateKey('89E6052886B5822223238F04FDA1246D61930FE69A90A1DDAE6131F948558024', SiriusConstants.networkType);

		const cosignerPrivateKeys: string[] = [ '15EB56E32A28643775C14027115F054977ED4F08A83E335046D78E4A10B4EE66', 'D34556B025D401D4F1BAE98D03CCBB8062573F36ECFBD7CD161878DB25903991' ];
		const cosigners = cosignerPrivateKeys.map(cosignerPrivateKey =>  Account.createFromPrivateKey(cosignerPrivateKey, SiriusConstants.networkType));

		const recipient = Address.createFromRawAddress('WDYTE5UI6KBB4NVQB6TYNAZ75ZTC54CQF6RPACKT');

		const transferTransaction = TransferTransaction.create(Deadline.create(), recipient, [], EmptyMessage, SiriusConstants.networkType);

		const dummyTxnAS1 = TransferTransaction.create(Deadline.create(), cosigners[0].address, [], EmptyMessage, SiriusConstants.networkType);
		const dummyTxnAS2 = TransferTransaction.create(Deadline.create(), cosigners[1].address, [], EmptyMessage, SiriusConstants.networkType);

    const aggregateComplete: AggregateTransaction = AggregateTransaction.createComplete(
			Deadline.create(15, ChronoUnit.MINUTES),
			[
				transferTransaction.toAggregate(msxAccount.publicAccount),
				dummyTxnAS1.toAggregate(cosigners[0].publicAccount),
				dummyTxnAS2.toAggregate(cosigners[1].publicAccount)
			],
			SiriusConstants.networkType,
			[]
    );
    const signedAggregateComplete = aggregateComplete.signTransactionWithCosignatories(userAccount, cosigners, SiriusConstants.generationHash);
    console.log('TCL: MultisigConversion -> convertToMultisigWithComplete -> signedAggregateBonded', signedAggregateComplete.hash);

		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		transactionHttp.announce(signedAggregateComplete).toPromise();

		this.checkTxnStatus(signedAggregateComplete.hash, (message: string) => {
			console.log('TCL: MultisigAggregateComplete -> init -> message', message);
		});
	}

	checkTxnStatus(hash: string, callback?: Function) {
		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		const timeout = setTimeout(() => {
			transactionHttp.getTransactionStatus(hash).subscribe(status => {
      console.log('TCL: MultisigAggregateComplete -> interval -> status', status)
				if(status.group === 'confirmed' || status.group === 'failed') {
					clearTimeout(timeout);
					callback('The transaction with the following hash is confirmed');
				} else {
					this.checkTxnStatus(hash);
				}
			}, err => {
				console.error('TCL: MosaicCreation -> init -> err', err);
				this.checkTxnStatus(hash);
				});
		}, 5000);
	}

	initWithSerialize() {
		// Wallet side
		// ========================================================================================================================================================
		// 0 - Prepare MSX and User account
		const msxAccount = Account.createFromPrivateKey('85B74CB59909F9CFC1CF063BE011157F48641C054D8C5BF1FEE61C4F84CC680B', SiriusConstants.networkType);
		const userAccount = Account.createFromPrivateKey('397082F92A2AABC85A3AA955620C833CB1ED906394D17A4DC550676D1FE42511', SiriusConstants.networkType);

		// 1 - Prepare Original Recipient
		const originalRecipient = Address.createFromRawAddress('WDYTE5UI6KBB4NVQB6TYNAZ75ZTC54CQF6RPACKT');

		// 2 - Prepare original transaction
		const originalTransferTxn = TransferTransaction.create(Deadline.create(), originalRecipient, [], EmptyMessage, SiriusConstants.networkType);

		// 3 - Prepare dummy transactions
		const dummyRecipients: string[] = [
			'EF3888929931E40B65FA91C59E964E6542EE5C99DA3F3F1BD2D6EBD452DDECB2',
			'08BD0BD02B783ECAC1A15EE6B013566BB99FC79A4454C4E5506DB49BB62AEE81',
		];
		// 4 - Prepare dummy inner transactions
		const dummyInnerTxns: InnerTransaction[] = dummyRecipients.map((dummyRecipient: string) => {
			const publicAccount = PublicAccount.createFromPublicKey(dummyRecipient, SiriusConstants.networkType);
			const dummyTransferTxn = TransferTransaction.create(Deadline.create(), originalRecipient, [], EmptyMessage, SiriusConstants.networkType);
			return dummyTransferTxn.toAggregate(publicAccount);
	});

    const aggregateComplete: AggregateTransaction = AggregateTransaction.createComplete(
			Deadline.create(15, ChronoUnit.MINUTES),
			[
				originalTransferTxn.toAggregate(msxAccount.publicAccount),
				originalTransferTxn.toAggregate(userAccount.publicAccount),
				...dummyInnerTxns
			],
			SiriusConstants.networkType,
			[]
		);
		const userSignedTransaction = userAccount.sign(aggregateComplete, SiriusConstants.generationHash);
		// ========================================================================================================================================================
		// End: Wallet side

		// Server side
		const autosign1Account = Account.createFromPrivateKey('15EB56E32A28643775C14027115F054977ED4F08A83E335046D78E4A10B4EE66', SiriusConstants.networkType);
		const autosign2Account = Account.createFromPrivateKey('D34556B025D401D4F1BAE98D03CCBB8062573F36ECFBD7CD161878DB25903991', SiriusConstants.networkType);

		const signedTxAS1 = CosignatureTransaction.signTransactionPayload(autosign1Account, userSignedTransaction.payload, SiriusConstants.generationHash);
		const signedTxAS2 = CosignatureTransaction.signTransactionPayload(autosign2Account, userSignedTransaction.payload, SiriusConstants.generationHash);

		const cosignatureSignedTransactions = [
			new CosignatureSignedTransaction(signedTxAS1.parentHash, signedTxAS1.signature, signedTxAS1.signer),
			new CosignatureSignedTransaction(signedTxAS2.parentHash, signedTxAS2.signature, signedTxAS2.signer),
		];
		const recreatedTxn: AggregateTransaction = TransactionMapping.createFromPayload(userSignedTransaction.payload) as AggregateTransaction;
		const signedAggregateComplete: SignedTransaction = recreatedTxn.signTransactionGivenSignatures(userAccount, cosignatureSignedTransactions, SiriusConstants.generationHash);

		const standardCosignedTransaction: SignedTransaction = aggregateComplete.signTransactionWithCosignatories(userAccount, [ autosign1Account, autosign2Account ], SiriusConstants.generationHash);

		const signedAggregateCompleteDecoded: AggregateTransaction = TransactionMapping.createFromPayload(signedAggregateComplete.payload) as AggregateTransaction;
		const standardSignedAggregateCompleteDecoded: AggregateTransaction = TransactionMapping.createFromPayload(standardCosignedTransaction.payload) as AggregateTransaction;

		console.log('TCL: MultisigAggregateComplete -> initWithSerialize -> standardSignedAggregateCompleteDecoded', standardSignedAggregateCompleteDecoded);
		console.log('TCL: MultisigAggregateComplete -> initWithSerialize -> hash', standardCosignedTransaction.hash === signedAggregateComplete.hash);
		console.log('TCL: MultisigAggregateComplete -> initWithSerialize -> payload', standardCosignedTransaction.payload === signedAggregateComplete.payload);
		console.log('TCL: MultisigAggregateComplete -> initWithSerialize -> signedAggregateCompleteDecoded', signedAggregateCompleteDecoded);
		console.log('================================================================================================================');
		console.log('TCL: MultisigAggregateComplete -> initWithSerialize -> signedAggregateComplete', signedAggregateComplete)
    	console.log('TCL: MultisigAggregateComplete -> initWithSerialize -> standardCosignedTransaction', standardCosignedTransaction)

		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		transactionHttp.announce(standardCosignedTransaction).toPromise().then(_ => _);
		// End: Server side

		const interval = setInterval(() => {
			transactionHttp.getTransactionStatus(standardCosignedTransaction.hash).subscribe(status => {
				console.log('TCL: MultisigAggregateComplete -> signedAggregateComplete -> status', status)
				if(status.group === 'confirmed' || status.group === 'failed') {
					clearInterval(interval);
				}
			}, err => {
      	console.error('TCL: MosaicCreation -> init -> err', err);
			});
		}, 5000);
	}
}
