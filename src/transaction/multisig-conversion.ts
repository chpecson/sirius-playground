import { filter, map, mergeMap, delay, flatMap, take } from 'rxjs/operators';
import {
  Account,
  AggregateTransaction,
  CosignatureSignedTransaction,
  CosignatureTransaction,
  Deadline,
  EmptyMessage,
  HashLockTransaction,
  Listener,
  ModifyMultisigAccountTransaction,
  Mosaic,
  MosaicId,
  MultisigCosignatoryModification,
  MultisigCosignatoryModificationType,
  PublicAccount,
  TransactionHttp,
  TransactionType,
  TransferTransaction,
  UInt64,
	AccountHttp,
	SignedTransaction,
} from 'tsjs-xpx-chain-sdk';

import { SiriusConstants } from '../@constants/sirius.constant';
import {yamprint} from 'yamprint';

import * as signale from 'signale';
import process from 'process';
import { ChronoUnit } from 'js-joda';
import { interval } from 'rxjs';

export class MultisigConversion {

	listener: Listener;
	accountHttp: AccountHttp;
	transactionHttp: TransactionHttp;
	signedTxns: Array<{ hash: string, address: string }> = [];
	interval: number;

	constructor() {
		this.accountHttp = new AccountHttp(SiriusConstants.nodeUrl);
		this.transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		this.listener = new Listener(SiriusConstants.wsNodeUrl);

		process.on('SIGINT', () => {
			signale.debug('SIGINT fired');
			this.listener.close()
		});
	}

	init() {
		this.listener.open().then(_ => {
			this.initConversion();
		});
	}

	initConvertWithAggregateComplete() {
		const msuAccount = Account.generateNewAccount(SiriusConstants.networkType);
		const chriAccount = Account.generateNewAccount(SiriusConstants.networkType);
		const josephAccount = Account.generateNewAccount(SiriusConstants.networkType);
		this.convertToMultisigWithComplete(msuAccount, [ chriAccount, josephAccount ], 1, 1);

		this.prettyLogAccount(msuAccount, 'msuAccount');
		this.prettyLogAccount(chriAccount, 'chriAccount');
		this.prettyLogAccount(josephAccount, 'josephAccount');
	}

	initConversion() {
		const interimA = PublicAccount.createFromPublicKey('92580839A3BABDCE8F8BA41C46D2E1E444A91939EDC0DEF4E9069B850A035070', SiriusConstants.networkType);
		const interimB = PublicAccount.createFromPublicKey('92EA2F16E8FEE353C06677C2CA0D3A18C81453550BB5887019604BF3F6CF27DC', SiriusConstants.networkType);

		const autosign1 = PublicAccount.createFromPublicKey('CC15FE8A4BCED0A7C0E73FF7C9386326A29269031B8A2EEEE4C783B56A33025B', SiriusConstants.networkType);
		const autosign2 = PublicAccount.createFromPublicKey('9A45517044F260A4A4A5A7294910B6F034A60C1614B4A9D6C6CF3AD069D062C9', SiriusConstants.networkType);
		const manager1 = PublicAccount.createFromPublicKey('3D60B2142D536CB16F9EF384546195820C9B48E72579B46F2AD54C480D7AAB44', SiriusConstants.networkType);
		const staff1 = PublicAccount.createFromPublicKey('E9444338259D3C16DB580AC405DFFE17306ACC23922B6FD11B1C47A81ED4122B', SiriusConstants.networkType);

		// 1st set - msx, msu, userAccount
		const msxAccount = Account.generateNewAccount(SiriusConstants.networkType);
		const msuAccount = Account.generateNewAccount(SiriusConstants.networkType);
		const userAccount = Account.generateNewAccount(SiriusConstants.networkType);

		this.prettyLogAccount(msxAccount, 'msxAccount');
		this.prettyLogAccount(msuAccount, 'msuAccount');
		this.prettyLogAccount(userAccount, 'userAccount');

		// 1st set multisig conversion
		this.convertToMultisigWithComplete(msuAccount, [ userAccount ], 1, 1);
		this.autosignHttpInterval(userAccount, 'userAccount');

		// 2nd set multisig conversion
		const msxConfig = {
			cosigners: [ msuAccount.publicAccount, interimA, interimB ],
			dummyRecipients: [ autosign1, autosign2, manager1, staff1, userAccount.publicAccount ],
		};

		this.convertToMultisig(msxAccount, msxConfig.cosigners, msxConfig.dummyRecipients, 1, 1);
	}

	convertToMultisig(toConvert: Account, cosigners: PublicAccount[], dummyRecipients: PublicAccount[] = [], minAprroval: number = 1, minRemoval: number = 1, deadlineInMinutes: number = 10) {

		this.listener.open().then(() => {
			const cosignerModifications = cosigners.map(cosigner => new MultisigCosignatoryModification(MultisigCosignatoryModificationType.Add, cosigner));
			const convertIntoMultisigTransaction = ModifyMultisigAccountTransaction.create(Deadline.create(15, ChronoUnit.MINUTES), minAprroval, minRemoval, cosignerModifications, SiriusConstants.networkType, new UInt64([0, 0]));

			const dummyTxns = dummyRecipients.length === 0 ? [] : dummyRecipients.map(dummyRecipient => TransferTransaction.create(Deadline.create(15, ChronoUnit.MINUTES), dummyRecipient.address, [], EmptyMessage, SiriusConstants.networkType, new UInt64([0, 0])));
			const dummyInnerTxns = dummyTxns.length === 0 ? [] : dummyRecipients.map((dummyRecipient, index) => {
				const dummyTxn = dummyTxns[index];
				return dummyTxn.toAggregate(dummyRecipient);
			});

			const aggregateBonded: AggregateTransaction = AggregateTransaction.createBonded(
				Deadline.create(15, ChronoUnit.MINUTES),
				[ convertIntoMultisigTransaction.toAggregate(toConvert.publicAccount), ...dummyInnerTxns ],
				SiriusConstants.networkType,
				[],
				new UInt64([0, 0])
			);
      console.log('TCL: MultisigConversion -> convertToMultisig -> aggregateBonded.networkType', aggregateBonded.networkType)
			const signedAggregateBonded = toConvert.signTransactionWithCosignatories(aggregateBonded, [], SiriusConstants.generationHash);

			const lockFunds = HashLockTransaction.create(
				Deadline.create(15, ChronoUnit.MINUTES),
				new Mosaic(new MosaicId(SiriusConstants.mosaicHexId), UInt64.fromUint(0)),
				UInt64.fromUint(128),
				signedAggregateBonded,
				SiriusConstants.networkType,
				new UInt64([0, 0])
			);
			const signedLockFunds = toConvert.sign(lockFunds, SiriusConstants.generationHash);

			console.log('TCL: MultisigConversion -> convertToMultisig -> signedLockFunds', signedLockFunds.hash);
			console.log('TCL: MultisigConversion -> convertToMultisig -> signedAggregateBonded', signedAggregateBonded.hash);

			this.listener.status(toConvert.address).subscribe(err => console.log('TCL: MultisigConversion -> convertToMultisig -> err', err));
			this.listener.confirmed(toConvert.address).pipe(
				filter(transaction => transaction.transactionInfo !== undefined && transaction.transactionInfo.hash === signedLockFunds.hash),
				mergeMap((ignored) => this.transactionHttp.announceAggregateBonded(signedAggregateBonded))
			).subscribe(txnStatus => {
				console.log(new Date, 'TCL: MultisigConversion -> convertToMultisig -> txnStatus', yamprint(txnStatus));
			});

			this.transactionHttp.announce(signedLockFunds).subscribe(_ => _);
		});
	}

	convertToMultisigWithComplete(toConvert: Account, cosigners: Account[], minAprroval: number = 1, minRemoval: number = 1, deadlineInMinutes: number = 15) {
		const cosignerModifications = cosigners.map(cosigner => new MultisigCosignatoryModification(MultisigCosignatoryModificationType.Add, cosigner.publicAccount));
		const convertIntoMultisigTransaction = ModifyMultisigAccountTransaction.create(Deadline.create(deadlineInMinutes, ChronoUnit.MINUTES), minAprroval, minRemoval, cosignerModifications, SiriusConstants.networkType);

		const aggregateComplete: AggregateTransaction = AggregateTransaction.createComplete(
			Deadline.create(deadlineInMinutes, ChronoUnit.MINUTES),
			[ convertIntoMultisigTransaction.toAggregate(toConvert.publicAccount) ],
			SiriusConstants.networkType,
			[],
			new UInt64([0, 0])
		);
		const signedAggregateComplete = toConvert.signTransactionWithCosignatories(aggregateComplete, cosigners, SiriusConstants.generationHash);
    console.log('TCL: MultisigConversion -> convertToMultisigWithComplete -> signedAggregateComplete.networkType', signedAggregateComplete.networkType)
    console.log('TCL: MultisigConversion -> convertToMultisigWithComplete -> signedAggregateComplete', signedAggregateComplete)
		signale.debug('TCL: MultisigConversion -> convertToMultisig -> signedAggregateComplete', yamprint(signedAggregateComplete));

		this.checkTxnStatus(signedAggregateComplete);

		this.transactionHttp.announce(signedAggregateComplete).subscribe(res => {
    	console.log('TCL: MultisigConversion -> convertToMultisigWithComplete -> res', res);
		});
	}

	autosignMultisigTxn(account: Account, label: string = '') {
		this.listener.open().then(() => {
			this.listener.aggregateBondedAdded(account.address).pipe(
				filter(txn => txn.type === TransactionType.AGGREGATE_BONDED),
				map(txn => txn as AggregateTransaction),
				filter(txn => txn.innerTransactions[0].type === TransactionType.MODIFY_MULTISIG_ACCOUNT),
				delay(3000),
				map(aggregateTxn => {
					if(!this.signedTxns.find(signedTxn => signedTxn.hash === aggregateTxn.transactionInfo.hash && signedTxn.address === account.address.plain())) {
						this.signedTxns.push({ hash: aggregateTxn.transactionInfo.hash, address: account.address.plain() });

						if(!aggregateTxn.signedByAccount(account.publicAccount)) {
							const cosignedTxn = this.cosignTransaction(aggregateTxn, account);
							this.transactionHttp.announceAggregateBondedCosignature(cosignedTxn).subscribe(txnStatus => {

								signale.success(`TCL: MultisigConversion -> autosignMultisigTxn -> ${label} -> hash`, aggregateTxn.transactionInfo.hash);
								signale.success(`TCL: MultisigConversion -> autosignMultisigTxn -> ${label} -> address`, account.publicKey);
								signale.success(`TCL: MultisigConversion -> autosignMultisigTxn -> ${label} -> txnStatus`, txnStatus.message);
							});
						}
					}
				})
			).subscribe(_ => _, err => {
			signale.error('TCL: MultisigConversion -> autosignMultisigTxn -> err', err);
			});
		});
	}

	autosignHttpInterval(account: Account, label: string = '') {
		const interval1 = interval(5000).pipe(take(100));

		interval1.subscribe(() => {
			this.accountHttp.aggregateBondedTransactions(account.publicAccount).pipe(
				flatMap(txn => txn),
				filter(txn => txn.type === TransactionType.AGGREGATE_BONDED),
				map(txn => txn as AggregateTransaction),
				filter(txn => txn.innerTransactions[0].type === TransactionType.MODIFY_MULTISIG_ACCOUNT),
				delay(3000),
				map(aggregateTxn => {
					if(!this.signedTxns.find(signedTxn => signedTxn.hash === aggregateTxn.transactionInfo.hash && signedTxn.address === account.address.plain())) {
						this.signedTxns.push({ hash: aggregateTxn.transactionInfo.hash, address: account.address.plain() });

						if(!aggregateTxn.signedByAccount(account.publicAccount)) {
							const cosignedTxn = this.cosignTransaction(aggregateTxn, account);
							this.transactionHttp.announceAggregateBondedCosignature(cosignedTxn).subscribe(txnStatus => {

								signale.success(`TCL: MultisigConversion -> autosignMultisigTxn -> ${label} -> hash`, aggregateTxn.transactionInfo.hash);
								signale.success(`TCL: MultisigConversion -> autosignMultisigTxn -> ${label} -> address`, account.publicKey);
								signale.success(`TCL: MultisigConversion -> autosignMultisigTxn -> ${label} -> txnStatus`, txnStatus.message);
							});
						}
					}
				})
			).subscribe(_ => _, err => {
				signale.error('TCL: MultisigConversion -> autosignMultisigTxn -> err', err);
			});
		})
	}

	autosignHttpInterval2(listenToAccount: PublicAccount, signerAccount: Account, label: string = '') {
		const interval2 = interval(5000).pipe(take(100))
		interval2.subscribe(() => {
			this.accountHttp.aggregateBondedTransactions(listenToAccount).pipe(
				flatMap(txn => txn),
				filter(txn => txn.type === TransactionType.AGGREGATE_BONDED),
				map(txn => txn as AggregateTransaction),
				filter(txn => txn.innerTransactions[0].type === TransactionType.MODIFY_MULTISIG_ACCOUNT),
				delay(3000),
				map(aggregateTxn => {
					if(!this.signedTxns.find(signedTxn => signedTxn.hash === aggregateTxn.transactionInfo.hash && signedTxn.address === signerAccount.address.plain())) {
						this.signedTxns.push({ hash: aggregateTxn.transactionInfo.hash, address: signerAccount.address.plain() });

						if(!aggregateTxn.signedByAccount(signerAccount.publicAccount)) {
							const cosignedTxn = this.cosignTransaction(aggregateTxn, signerAccount);
							this.transactionHttp.announceAggregateBondedCosignature(cosignedTxn).subscribe(txnStatus => {

								signale.success(`TCL: MultisigConversion -> autosignMultisigTxn -> ${label} -> hash`, aggregateTxn.transactionInfo.hash);
								signale.success(`TCL: MultisigConversion -> autosignMultisigTxn -> ${label} -> address`, signerAccount.publicKey);
								signale.success(`TCL: MultisigConversion -> autosignMultisigTxn -> ${label} -> txnStatus`, txnStatus.message);
							});
						}
					}
				})
			).subscribe(_ => _, err => {
				signale.error('TCL: MultisigConversion -> autosignMultisigTxn -> err', err);
			});
		})
	}

	cosignTransaction(transaction: AggregateTransaction, account: Account): CosignatureSignedTransaction {
		const cosignatureTransaction = CosignatureTransaction.create(transaction);
		return account.signCosignatureTransaction(cosignatureTransaction);
	};

	prettyLogPublicAccount(account: PublicAccount, label: string = '') {
		signale.debug(`TCL: MultisigConversion -> prettyLogAccount -> ${label} -> ${account.publicKey} -> ${account.address.pretty()}`);
	}

	prettyLogAccount(account: Account, label: string = '') {
		signale.debug(`============================================================================`);
		signale.debug(`TCL: MultisigConversion -> prettyLogAccount -> ${label} -> ${label}`);
		signale.debug(`TCL: MultisigConversion -> prettyLogAccount -> ${label} -> address -> ${account.address.plain()}`);
		signale.debug(`TCL: MultisigConversion -> prettyLogAccount -> ${label} -> privateKey -> ${account.privateKey}`);
		signale.debug(`TCL: MultisigConversion -> prettyLogAccount -> ${label} -> publicKey -> ${account.publicKey}`);
	}

	checkTxnStatus(txn: SignedTransaction) {
		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		const interval = setTimeout(() => {
		 transactionHttp.getTransactionStatus(txn.hash).subscribe(status => {
			 console.log('TCL: MultisigConversion -> init -> status ', status);
			 if(status.group === 'confirmed') {
				 clearTimeout(interval);
			 } else {
				 clearTimeout(interval);
				 this.checkTxnStatus(txn);
			 }
		 }, err => {
				console.error('TCL: MultisigConversion -> init -> err', err);
				 clearTimeout(interval);
				 this.checkTxnStatus(txn);
			});
	 }, 5000);
 }
}
