import {
	Account,
	Address,
	Deadline,
	Mosaic,
	MosaicId,
	PlainMessage,
	TransactionHttp,
	TransferTransaction,
	UInt64,
	PublicAccount,
	SignedTransaction
} from 'tsjs-xpx-chain-sdk';

// import { TransactionPrintService } from '../@console-print/transaction-print.service';
import { SiriusConstants } from '../@constants/sirius.constant';

export class SimpleTransfer {
	init() {
		const sender = Account.generateNewAccount(SiriusConstants.networkType);
		const recipient = Address.createFromRawAddress('WCDDJ45RUAHMCHG4SPNNWV3ZQ4ZWLXGGN3FJONPO');

		const transferTransaction = TransferTransaction.create(
			Deadline.create(),
			recipient,
			[
				// new Mosaic(new MosaicId('0b44b023791451a5'), UInt64.fromUint(1000000 * Math.pow(10, 4))),
				// new Mosaic(new MosaicId('51ca98a40a4136a4'), UInt64.fromUint(1 * Math.pow(10, 6)))
			],
			PlainMessage.create('Test mosaics'),
			SiriusConstants.networkType,
			new UInt64([0, 0])
		);
		// TransactionPrintService.prettyPrint(transferTransaction);

		const signedTxn = sender.sign(transferTransaction, SiriusConstants.generationHash);
		console.log('TCL: SimpleTransfer -> init -> signedTxn', signedTxn);

		const txnHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		txnHttp.announce(signedTxn).toPromise().then(_ => _);

		this.checkTxnStatus(signedTxn.hash, (result) => {
    	console.log('TCL: SimpleTransfer -> init -> result', result);
		});
	}

	checkTxnStatus(hash: string, callback?: Function) {
		const transactionHttp = new TransactionHttp(SiriusConstants.nodeUrl);
		const timeout = setTimeout(() => {
			transactionHttp.getTransactionStatus(hash).subscribe(status => {
      console.log('TCL: SimpleTransfer -> interval -> status', status)
				if(status.group === 'confirmed' || status.group === 'failed') {
					clearTimeout(timeout);
					callback('The transaction with the following hash is confirmed');
				} else {
					this.checkTxnStatus(hash);
				}
			}, err => {
				console.error('TCL: SimpleTransfer -> init -> err', err);
				this.checkTxnStatus(hash);
				});
		}, 5000);
	}
}
